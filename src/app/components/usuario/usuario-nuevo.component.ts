import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-usuario-nuevo',
  templateUrl: './usuario-nuevo.component.html',
  styleUrls: ['./usuario-nuevo.component.css']
})
export class UsuarioNuevoComponent implements OnInit {

  constructor(private router: ActivatedRoute) {
    this.router.params.subscribe(parametros => {
      console.log('Ruta hija - Usuario Nuevo');
      console.log(parametros);
    });
    this.router.parent?.params.subscribe(parametros => {
      console.log('Ruta Padre - Usuario Nuevo');
      console.log(parametros);
    });
   }

  ngOnInit(): void {
  }

}
